from django.urls import path
from .views import lab6, story2

urlpatterns = [
    path('', story2, name="story"),
    path('form', lab6, name="lab6")
]
