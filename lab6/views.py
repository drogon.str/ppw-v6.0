from django.shortcuts import render
from .forms import landingPage
from .models import model as model2

# Create your views here.
response = {'Author':"Hermawan Arifin"}

def story2(request):
    return render(request, 'story2.html')


def lab6(request):
    model = model2.objects.all()
    form = landingPage(request.POST)

    if(request.method == "POST"):
        response['statuses'] = request.POST['status']
        objMod = model2(statuses = response['statuses'])
        objMod.save()
        form = landingPage()
        context = {
            'form': form,
            'mod' : model
        }
        return render(request, 'lab6.html', context)

    else:
        form = landingPage()
        context ={
            'form': form,
            'mod' : model
        }
        return render(request, 'lab6.html', context)


