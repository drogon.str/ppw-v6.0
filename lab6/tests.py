from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import lab6
from .models import model
from .forms import landingPage
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import unittest
# Create your tests here.
class lab6Test(TestCase):
	def test_lab_6_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)
	
	def test_page_baru_url_is_exist(self):
		response = Client().get('/form')
		self.assertEqual(response.status_code, 200)

	def test_lab6_using_to_do_list_template(self):
		response = Client().get('/form')
		self.assertTemplateUsed(response, 'lab6.html')
	
	def test_hello(self):
		request = HttpRequest()
		resp = lab6(request)
		html_resp = resp.content.decode('utf8')
		self.assertIn('Hello, Apa kabar?', html_resp)
	
	def test_lab6_using_index_func(self):
		found = resolve('/form')
		self.assertEqual(found.func, lab6)
	
	def test_exist(self):
		status_baru = model.objects.create(statuses = "ppw ku sayang")
		amount = model.objects.all().count()
		self.assertEqual(amount,1)

class newLab7Test(unittest.TestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		service_log_path = "./chromedriver.log"
		service_args = ['--verbose']
		self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

	def tearDown(self):
		self.browser.quit()

	def test_can_start_a_list_and_retrieve_it_later(self):
		self.browser.get('http://localhost:8000/form')
		time.sleep(1)
		isi = self.browser.find_element_by_id('id_status')
		isi.send_keys('coba coba ea')
		isi.submit()
		time.sleep(3)
		self.assertIn('coba coba ea', self.browser.page_source)

